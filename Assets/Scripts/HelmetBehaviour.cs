using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelmetBehaviour : MonoBehaviour
{
    public Transform rotatingTransform;

    public void HelmetFlip()
    {
        rotatingTransform.localRotation = Quaternion.Euler(rotatingTransform.localRotation.x, rotatingTransform.localRotation.y, rotatingTransform.localEulerAngles.z + 180);
    }
}
