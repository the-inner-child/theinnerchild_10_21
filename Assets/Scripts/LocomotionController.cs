using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class LocomotionController : MonoBehaviour
{
    public XRController rightTeleportRay;
    public InputHelpers.Button teleportActivationButton;
    public float activationThreshold = 0.1f;

    public XRRayInteractor rightInteractorRay;
    public bool EnableRightTeleport { get; set; } = true;

    private void Update()
    {
        //Vector3 pos = new Vector3();
        //Vector3 norm = new Vector3();
        //int index = 0;
        //bool validTarget = false;

        if (rightTeleportRay)
        {
            bool isRightInteractorRayHovering = rightInteractorRay.TryGetHitInfo(out Vector3 pos, out Vector3 norm, out int index, out bool validTarget);

            rightTeleportRay.gameObject.SetActive(CheckIfActivated(rightTeleportRay) && EnableRightTeleport && !isRightInteractorRayHovering);
        }
    }

    public bool CheckIfActivated(XRController controller)
    {
        InputHelpers.IsPressed(controller.inputDevice, teleportActivationButton, out bool isActivated, activationThreshold);
        return isActivated;
    }


}
